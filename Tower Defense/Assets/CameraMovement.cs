﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{

    public float scrollSpeed = 15;
    private int scrollDistance = 10;

    void Update()
    {

        float mousePosX = Input.mousePosition.x;
        float mousePosY = Input.mousePosition.y;

        if (mousePosX < scrollDistance || Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(Vector3.right * -scrollSpeed * Time.deltaTime);
        }

        if (mousePosX >= Screen.width - scrollDistance || Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector3.right * scrollSpeed * Time.deltaTime);
        }

        if (mousePosY < scrollDistance || Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(transform.forward * scrollSpeed * Time.deltaTime);
        }

        if (mousePosY >= Screen.height - scrollDistance || Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(transform.forward * -scrollSpeed * Time.deltaTime);
        }
    }
}