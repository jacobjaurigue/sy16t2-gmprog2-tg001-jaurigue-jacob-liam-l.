﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerFunctions : MonoBehaviour
{
    public float Speed;
    public Vector3 size = new Vector3(1.0f, 1.0f, 0.0f);
    public Vector3 scale;
    public string Tag;
    public float Increase;
    public Text Letters;
    public float xMin, xMax, zMin, zMax;

    int Score = 0;

    void Update()
    {
        MouseMovement();
        transform.localScale = size;
        Scaling();

    }

    void MouseMovement()
    {
        Vector3 Target = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Target.z = transform.position.z;

        transform.position = Vector3.MoveTowards(transform.position, Target, Speed * Time.deltaTime / transform.localScale.x);
    }

    void Scaling()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            size += new Vector3(1.0f, 1.0f, 0.0f);
        }
        else if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            size -= new Vector3(1.0f, 1.0f, 0.0f);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == Tag)
        {
            size += new Vector3(Increase, Increase, Increase);
            Destroy(other.gameObject);
        }

        Camera.main.orthographicSize += 0.5f;
        scale += new Vector3(0.5f, 0, 0.5f);
        gameObject.transform.localScale = scale;

        Score += 10;
        Letters.text = "SCORE: " + Score;
    }
    
    void FixedUpdate()
    {

    }
}