﻿using UnityEngine;
using System.Collections;

public class CameraOrtho : MonoBehaviour
{

    public Transform LookAt;

    void Start()
    {

    }

    void Update()
    {
        Camera.main.orthographicSize = LookAt.transform.localScale.x + 5;
    }
}