﻿using UnityEngine;
using System.Collections;

public enum EnemyState
{
    patrol,
    chase,
    run
}

public class EnemyScript : MonoBehaviour
{
    public EnemyState enemyState = EnemyState.patrol;

    public GameObject target;
    private Vector3 randomTarget;
    private float speed = 5;
    public string Tag;
    public float Increase;

    void Update()
    {
        switch (enemyState)
        {
            case EnemyState.patrol:
                Patrol();
                break;
            case EnemyState.chase:
                Chase();
                break;
            case EnemyState.run:
                Run();
                break;
        }
        if (GetComponent<EnemyDetector>().otherEnemy != null)
        {
            target = GetComponent<EnemyDetector>().otherEnemy;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == Tag)
        {
            transform.localScale += new Vector3(Increase, Increase, Increase);
            Destroy(other.gameObject);
        }
    }

    void Patrol()
    {
        if (Vector3.Distance(this.transform.position, randomTarget) < 2)
        {
            float posX = Random.Range(-10, 10);
            float posY = Random.Range(-10, 10);
            randomTarget = new Vector3(posX, posY, 0);
        }

        this.transform.position = Vector3.MoveTowards(this.transform.position, randomTarget, Time.deltaTime * speed / transform.localScale.x);

        if (target != null)
        {
            if (target.transform.localScale.x < this.transform.localScale.x)
            {
                enemyState = EnemyState.chase;
            }
            else if (target.transform.localScale.x >= this.transform.localScale.x)
            {
                enemyState = EnemyState.run;
            }
        }
    }

    void Chase()
    {
        if (target != null && (Mathf.Abs(Vector3.Distance(this.transform.position, target.transform.position)) > 2) ||
            GetComponent<EnemyDetector>().otherEnemy == null)
        {
            target = null;
            enemyState = EnemyState.patrol;
        }
        else if (target.transform.localScale.x > this.transform.localScale.x)
        {
            target = null;
            enemyState = EnemyState.run;
        }
        else
        {
            this.transform.position =
                Vector3.MoveTowards(this.transform.position, target.transform.position, Time.deltaTime * speed);
        }
    }

    void Run()
    {
        if (target != null)
        {
            if (Vector3.Distance(this.transform.position, target.transform.position) > 5)
            {
                target = null;
                enemyState = EnemyState.patrol;
            }
            else
            {
                float altDir = (this.transform.position.x + this.transform.position.y) - (target.transform.position.x + target.transform.position.y);
                float moveToX = this.transform.position.x + altDir * Random.Range(10, 20);
                float moveToY = this.transform.position.y + altDir * Random.Range(10, 20);

                Vector3 moveTo = new Vector3(moveToX, moveToY, 0);
                this.transform.position =
                    Vector3.MoveTowards(this.transform.position, moveTo, Time.deltaTime * speed);
            }
        }
    }

}
