﻿using UnityEngine;
using System.Collections;


public class EnemyDetector : MonoBehaviour
{

    public GameObject otherEnemy;


    void OnTriggerEnter(Collider other)
    {
        if (otherEnemy == null)
            otherEnemy = other.gameObject;
    }
}
