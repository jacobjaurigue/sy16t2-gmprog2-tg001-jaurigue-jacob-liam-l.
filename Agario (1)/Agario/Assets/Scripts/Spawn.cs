﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour
{
    public GameObject Food;
    public float Speed;

    void Start()
    {
        for (int i = 0; i < 30; i++)
        {
            SpawnFood();
        }
        InvokeRepeating("SpawnFood", 0, Speed);
    }

    void SpawnFood()
    {
        int x = Random.Range(0, Camera.main.pixelWidth);
        int y = Random.Range(0, Camera.main.pixelHeight);

        Vector3 Target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0));
        Target.z = 0;

        Instantiate(Food, Target, Quaternion.identity);
    }
}